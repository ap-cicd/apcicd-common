import setuptools
import versioneer

with open("README.rst", "r") as fh:
    long_description = fh.read()
with open("requirements.txt", "r") as fh:
    requirements = [line.strip() for line in fh]

setuptools.setup(
    name="APCICD-common",
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    author="Sumit",
    author_email="sumit@email",
    description="A Python library which contains common utilities for automation tests",
    long_description=long_description,
    long_description_content_type="text/x-rst",
    url="https://gitlab.com/ap-cicd/apcicd-common",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=requirements,
)