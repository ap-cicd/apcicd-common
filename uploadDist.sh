#rm -f /root/.pypirc
echo "URL $1"
echo $2
echo $3
PYPI_REPO=$1
PYPI_USER=$2
PYPI_PASSWORD=$3
echo "[distutils]" >> /root/.pypirc
echo "index-servers = local" >> /root/.pypirc
echo "" >> /root/.pypirc
echo "[local]" >> /root/.pypirc
echo "repository:$1" >> /root/.pypirc
echo "username:$2" >> /root/.pypirc
echo "password:$3" >> /root/.pypirc
#echo "repository:XXX" >> /root/.pypirc
#echo "username:YYY@gmail.com" >> /root/.pypirc
#echo "password:ZZZ" >> /root/.pypirc
cat /root/.pypirc

python setup.py sdist upload -r local &&
python setup.py bdist_wheel upload -r local
