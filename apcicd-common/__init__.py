from .api_client import ApiClient
from .exceptions import DataError

__version__ = "0.0.2"
from . import _version
__version__ = _version.get_versions()['version']
