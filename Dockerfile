FROM python:3

# # USER app
# ARG PYPI_REPO
# ARG PYPI_USER
# ARG PYPI_PASSWORD

ENV PYTHONUNBUFFERED 1
# RUN mkdir /db
#RUN chown app:app -R /db

RUN mkdir /code
WORKDIR /code
# ADD ./requirements.txt /code
# RUN pip install -r requirements.txt
ADD . /code
# COPY entrypoint.sh /entrypoint.sh
RUN chmod +x ./uploadDist.sh
RUN echo PYPI_REPO
RUN echo PYPI_USER
RUN echo PYPI_PASSWORD
CMD ["/bin/echo 'Hello, world!'"]
entrypoint "./uploadDist.sh" ${PYPI_REPO} ${PYPI_USER} ${PYPI_PASSWORD}